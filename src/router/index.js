import { createRouter, createWebHashHistory } from "vue-router";
import CrearUsuarios from "../components/CrearUsuarios.vue";
import ListaUsuarios from "../components/ListaUsuarios.vue";
import ActualizarUsuarios from "../components/ActualizarUsuarios.vue";
import createClient from "../components/CrearClientes.vue";
import createAdmin from "../components/CrearAdmin.vue";
import createProduct from "../components/CrearProducto.vue";
import listClients from "../components/ListaClientes.vue";
import listAdmins from "../components/ListaAdmin.vue";
import listProducts from "../components/ListaProducto.vue";
import updateClients from "../components/ActualizarClientes.vue";
import updateAdmin from "../components/ActualizarAdmin.vue";
import updateProduct from "../components/ActualizarProductos.vue";

const routes = [
  {
    path: "/crear",
    name: "crearU",
    component: CrearUsuarios,
  },
  {
    path: "/listar",
    name: "listarU",
    component: ListaUsuarios,
  },
  {
    path: "/actualizar",
    name: "actualizarU",
    component: ActualizarUsuarios,
    props: true,
  },
  {
    path: "/admin/make-client",
    name: "Crear Cliente",
    component: createClient,
  },

  {
    path: "/admin/make-admin",
    name: "Crear Admin",
    component: createAdmin,
  },

  {
    path: "/admin/make-product",
    name: "Crear Producto",
    component: createProduct,
  },

  {
    path: "/admin/all-admin",
    name: "Listar Admin",
    component: listAdmins,
  },

  

  {
    path: "/admin/all-client",
    name: "Listar Clientes",
    component: listClients,
  },

  {
    path: "/admin/all-product",
    name: "Listar Productos",
    component: listProducts,
  },

  {
    path: "/admin/update-client",
    name: "Actualizar Cliente",
    component: updateClients,
  },

  {
    path: "/admin/update-admin",
    name: "Actualizar Admin",
    component: updateAdmin,
  },

  {
    path: "/admin/update-product",
    name: "Actualizar Producto",
    component: updateProduct,
  },

  {
    path: '/usuarios',
    name: 'Usuarios',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Usuarios.vue')
  },

  {
    path: '/clientes',
    name: 'Clientes',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Clientes.vue')
  },

  {
    path: '/admin',
    name: 'Admin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Admin.vue')
  },
  {
    path: '/productos',
    name: 'Productos',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Productos.vue')
  },

  {
    path: '/irlogin', //url
    name: 'Irlogin', //nombre en la funcion en el componente
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Irlogin.vue')   //nombre en formulario componente
  },

 
  
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
