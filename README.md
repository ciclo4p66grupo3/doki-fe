# doki_fe

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

###  GIT
```
git init
git branch namebranch
git checkout namebranch
git add .
git status
git commit -m'added a new quote'
git pull namebranch
git push namebranch
```
